package com.ucb.appin.views.fragment;

import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trello.rxlifecycle2.android.RxLifecycleAndroid;
import com.trello.rxlifecycle2.components.RxFragment;
import com.ucb.appin.R;
import com.ucb.appin.data.api.client.InMobService;
import com.ucb.appin.data.api.client.InMovServiceLocalImpl;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.util.ConstInVirtual;
import com.ucb.appin.views.dialogs.DialogDetailLocation;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import io.realm.Sort;

public class FragmentMappViewImpl extends RxFragment implements OnMapReadyCallback {
    private OnFragmentInteractionListener mListener;
    private View view;
    private View viewDetail;
    private Realm realm;
    private FragmentManager fragmentManager;
    private GoogleMap googleMap;
    private MapView mapView;
    private InMobService inMobService;
    private int zoomInit = 0;

    public FragmentMappViewImpl() {
        // Required empty public constructor
    }

    public static FragmentMappViewImpl newInstance(String param1, String param2) {
        FragmentMappViewImpl fragment = new FragmentMappViewImpl();
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_map_view, container, false);
        this.viewDetail = inflater.inflate(R.layout.fragment_publication_detail, container, false);
        fragmentManager = getFragmentManager();

        mapView = (MapView) this.view.findViewById(R.id.content_map);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        return this.view;
    }


    public List<AvisosBean> searchListPublication(String tBuscar) {
        this.inMobService = new InMovServiceLocalImpl();
        this.realm = Realm.getDefaultInstance();
        List<AvisosBean> listAvisos = new ArrayList<>();
        dispose();
        io.reactivex.Observable<List<AvisosBean>> observable = this.inMobService.listAvisos(new BitacoraBean());
        try {
            Disposable disposable = observable.compose(RxLifecycleAndroid.bindFragment (lifecycle()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable(BackpressureStrategy.BUFFER)
                    .switchMap(textChangeEvent -> {
                        // Use Async API to move Realm queries off the main thread.
                        return realm.where(AvisosBean.class)
                                //.contains ("titulo", tBuscar, Case.INSENSITIVE)
                                .findAllSortedAsync("id", Sort.DESCENDING)
                                .asFlowable();
                    })
                    .filter(notification -> notification.isLoaded())
                    .subscribe(data -> {
                                Log.v("==>", "=>RETURN IN EQUIP>" + data.size());
                                loadPublications(data);
                            },
                            error -> {
                                Log.v("==>", "=>ERROR EQUIP>" + error.getMessage());
                            });

            ConstInVirtual.disposable.add(disposable);
        } catch (Exception er) {
            Log.i("---ERROR--->", ">>" + er.getMessage());
        }
        return  listAvisos;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity().getApplicationContext());
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(ConstInVirtual.DEFAULT_LATITUDE, ConstInVirtual.DEFAULT_LONGITUDE)).zoom(ConstInVirtual.DEFAULT_ZOOM).build();
        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        searchListPublication("");
    }



    public void loadPublications(List<AvisosBean> listPublication) {
        if(listPublication != null){
            if(listPublication.size() > 0){
                AvisosBean ubicacionesBean  = listPublication.get(0);
                CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(ubicacionesBean.getLatitud(),ubicacionesBean.getLongitud()));
                this.googleMap.moveCamera(center);
                if(zoomInit == 0){
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(ConstInVirtual.DEFAULT_ZOOM);
                    this.googleMap.animateCamera(zoom);
                    zoomInit++;
                }
            }

            for (int i = 0;i < listPublication.size() - 1;i++){
                AvisosBean ubicacionesBean = listPublication.get(i);
                //observables
                drawMarker(ubicacionesBean, true);
            }

            if(listPublication.size() > 0){
                AvisosBean ubicacionesBean = listPublication.get(listPublication.size() - 1);
                drawMarker(ubicacionesBean, false);
            }
        }
    }


    public void drawMarker(AvisosBean avisosBean, boolean wicon) {
        if (googleMap != null) {
            MarkerOptions marker = new MarkerOptions().position(new LatLng(avisosBean.getLatitud(), avisosBean.getLongitud())).title("Detalle InMov").snippet("(Toque para detalles)");

            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    AvisosBean avisosBean = (AvisosBean) marker.getTag();
                    //showDetailsMarker(avisosBean);
                    showAlertDialog(avisosBean);
                }
            });
            if(wicon){
                markerDraw(marker, avisosBean);
            }
            else{
                markerDraw(marker, avisosBean);
            }
            Marker rmarker = googleMap.addMarker(marker);
            rmarker.setTag(avisosBean);
        }
    }

    private void markerDraw(MarkerOptions marker, AvisosBean avisosBean) {
        if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_HOME){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_home_map));
        }else if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_APPARTAMENT){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_appartament));
        }else if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_SINGLE_ROOM){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_singleroom));
        }else if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_SHOPING_LOCAL){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_shoping));
        }else if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_OFFICE){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_office));
        }else if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_BUILDING){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_building));
        }else if(avisosBean!=null&&avisosBean.getTipoaviso()==ConstInVirtual.TYPE_OTHER){
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_other_map));
        }
    }

    public void doPositiveClick() {
        // Do stuff here.
        Log.i("FragmentAlertDialog", "Positive click!");
    }


    private void showAlertDialog(AvisosBean data) {
        DialogDetailLocation alertDialog = DialogDetailLocation.newInstance(ConstInVirtual.DETAIL_LOCATION,data);
        alertDialog.show(fragmentManager, "fragment_alert");
    }


    //method for show popup
    private void showDetailsMarker(AvisosBean avisosBean){
        String scontent = "DIRECCION:" + avisosBean.getDireccion()
                + "\n TITULO:"+ avisosBean.getTitulo() + "," + avisosBean.getLongitud()
                + "\n PRECIO:" + avisosBean.getPrecio()
                + "\n DESC:" + avisosBean.getDescripcion()
                + "\n TELF:" + avisosBean.getTelefono()
                + "\n FECHA:" + avisosBean.getFecPublicacion();

//        TextView txtContent = (TextView) this.viewDetail.findViewById(R.id.txt_content_det);
//        txtContent.setText("t");

        Button btnSDetailMon = (Button) this.viewDetail.findViewById(R.id.btn_s_detail_det);

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity().getApplicationContext());
        mBuilder.setView(this.viewDetail);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnSDetailMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void dispose() {
        try {
            ConstInVirtual.disposable.clear();
        } catch (Exception er) {

        }
    }
}
