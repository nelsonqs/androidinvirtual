package com.ucb.appin.views.fragment;

import com.ucb.appin.data.model.AvisosBean;

import java.util.List;

/**
 * Created by tecnosim on 2/9/2018.
 */

public interface FragmentPublication {

    void showProgress(boolean b);
    void loadData(List<AvisosBean> listAvisos);
    void finish();
    void reloadData();
    void showDetailPublication(AvisosBean avisosBean);
    void searchPublication (String searchText);

}
