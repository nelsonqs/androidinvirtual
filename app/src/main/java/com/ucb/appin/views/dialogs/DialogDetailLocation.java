package com.ucb.appin.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.ucb.appin.R;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.util.ConstInVirtual;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by tecnosim on 2/24/2018.
 */
public class DialogDetailLocation extends DialogFragment  {
    AvisosBean avisosBean = new AvisosBean();

    public DialogDetailLocation() {
        // Empty constructor required for DialogFragment
    }




    public static DialogDetailLocation newInstance(String title, AvisosBean data) {
        DialogDetailLocation frag = new DialogDetailLocation();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("desc", data.getDescripcion());
        args.putString("precio", data.getPrecio() + "");
        args.putString("dir", data.getDireccion());
        args.putString("telf", data.getTelefono());
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = data.getFecPublicacion() != null ? df.format(data.getFecPublicacion()) : ConstInVirtual.WITHOUT_DATE;
        args.putString("fecha", fecha);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String description = getArguments().getString("desc");
        String precio = getArguments().getString("precio");
        String direccion = getArguments().getString("dir");
        String telf = getArguments().getString("telf");
        String fecha = getArguments().getString("fecha");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        avisosBean = new AvisosBean();

        String scontent = "Dir:" + direccion
                + "\nPrecio:" + precio
                + "\nDesc:" + description
                + "\nTelf:" + telf
                + "\nFecha:" + fecha;

        alertDialogBuilder.setMessage(scontent);
        alertDialogBuilder.setIcon(R.drawable.ic_action_call);
        alertDialogBuilder.setNeutralButton("LLamar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    callIntent.setData(Uri.parse("tel:" + telf));
                    startActivity(callIntent);
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), ConstInVirtual.MESSAGE_CALL_PHONE_FAIL,
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

        });

        return alertDialogBuilder.create();
    }

}
