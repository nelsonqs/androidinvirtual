package com.ucb.appin.views.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ucb.appin.R;
import com.ucb.appin.data.api.client.InMobService;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.util.ConstInVirtual;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class ActivityDetailPublicationImpl extends AppCompatActivity implements ActivityDetailPublication {

    private String path_foto = null;
    private List<TipoAvisoBean> listTipoAvisoBeans = new ArrayList<TipoAvisoBean>();
    private List<TransaccionAviso> listTransaccionAvisos = new ArrayList<TransaccionAviso>();
    private InMobService inMobService;    private String[]   slistaTipoAvisos;
    private String[]   slistaTransaccionAvisos;
    private AvisosBean avisosBean = null;
    private Realm realm;
    private double latitud = ConstInVirtual.DEFAULT_LATITUDE;
    private double longitud = ConstInVirtual.DEFAULT_LONGITUDE;

    // UI references.
    @BindView(R.id.detail_progress)
    View mProgressView;

    @BindView(R.id.detail_form)
    View mAddFormView;

    @BindView(R.id.img_detail_foto)
    ImageView imgAddFoto;

    @BindView(R.id.map_detail_ubicacion)
    MapView mapView;

    TextView txtDescription;
    TextView txtPrice;
    TextView txtPhoneNumber;
    TextView txtAddress;
    TextView txtTypePublication;
    TextView txtTransaction;
    ImageView viewAddFoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_publication_view);
        ButterKnife.bind(this);
        // recuperando datos desde la vista
        this.mProgressView = findViewById(R.id.detail_progress);
        this.mAddFormView = findViewById(R.id.detail_form);
        this.imgAddFoto = (ImageView) findViewById(R.id.img_detail_foto);
        txtDescription = (TextView) findViewById(R.id.txt_detail_descripcion);
        txtPrice = (TextView) findViewById(R.id.txt_detail_precio);
        txtPhoneNumber = (TextView) findViewById(R.id.txt_detail_telefono);
        txtAddress = (TextView) findViewById(R.id.txt_detail_direccion);
        txtTypePublication = (TextView) findViewById(R.id.txt_detail_tipo);
        txtTransaction = (TextView) findViewById(R.id.txt_detail_transaction);
        mapView.onCreate(savedInstanceState);
        loadGoogleMaps();

        viewAddFoto = (ImageView) findViewById(R.id.img_detail_foto);

        Gson gson = new Gson();

        Bundle extras = getIntent().getExtras();
        String tmp = extras.getString("avisosBean");
        this.avisosBean = gson.fromJson(tmp, AvisosBean.class);

        if(this.avisosBean != null){
            this.insertData();
        }

    }

    private void insertData(){
        Log.v("this.avoso", String.valueOf(this.avisosBean));
        this.txtDescription.setText("  "+this.avisosBean.getDescripcion());
        this.txtPrice.setText("  "+this.avisosBean.getPrecio().toString());
        this.txtPhoneNumber.setText("  "+this.avisosBean.getTelefono());
        this.txtAddress.setText("  "+this.avisosBean.getDireccion());
        this.txtTypePublication.setText(this.avisosBean.getTipoaviso()!=null?"  "+getTypePublication(this.avisosBean.getTipoaviso()):"Sin tipo" );
        this.txtTransaction.setText(this.avisosBean.getTransaccionaviso()!=null?"  "+getTransaction(this.avisosBean.getTransaccionaviso()):"Sin Transaccion");

        if(this.avisosBean.getImagen() != null){
            if(!this.avisosBean.getImagen().isEmpty()){
                byte[] imaget64 = Base64.decode(this.avisosBean.getImagen(), Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imaget64, 0, imaget64.length);
                imgAddFoto.setImageBitmap(decodedImage);
            }
        }

        try {
            this.latitud = this.avisosBean.getLatitud();
            this.longitud = this.avisosBean.getLongitud();
            loadGoogleMaps();
        }catch (Exception err){

        }
    }
    private String getTransaction(Integer tipoaviso) {
        String data ="";
        if ( tipoaviso.intValue() == ConstInVirtual.TYPE_SALES_TX) {
            return ConstInVirtual.TYPE_SALES_TX_STRING;
        }else if ( tipoaviso.intValue() == ConstInVirtual.TYPE_SALES_RESUMME_TX) {
            return ConstInVirtual.TYPE_SALES_RESUMME_TX_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_LEND_TX) {
            return ConstInVirtual.TYPE_LEND_TX_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_OTHER) {
            return ConstInVirtual.TYPE_OTHER_STRING;
        }
        return data;
    }

    private String getTypePublication(Integer tipoaviso) {
        String data ="";
        if ( tipoaviso.intValue() == ConstInVirtual.TYPE_HOME) {
           return ConstInVirtual.TYPE_HOME_STRING;
        }else if ( tipoaviso.intValue() == ConstInVirtual.TYPE_APPARTAMENT) {
            return ConstInVirtual.TYPE_APPARTAMENT_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_SINGLE_ROOM) {
            return ConstInVirtual.TYPE_SINGLE_ROOM_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_SHOPING_LOCAL) {
            return ConstInVirtual.TYPE_SHOPING_LOCAL_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_OFFICE) {
            return ConstInVirtual.TYPE_OFFICE_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_BUILDING) {
            return ConstInVirtual.TYPE_BUILDING_STRING;
        }if ( tipoaviso.intValue() == ConstInVirtual.TYPE_OTHER) {
            return ConstInVirtual.TYPE_OTHER_STRING;
        }
        return data;
    }


    /**
     * Shows the progress UI and hides the login form.
     */

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mAddFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mAddFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mAddFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mAddFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    private void loadGoogleMaps() {
        mapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(final GoogleMap googleMap) {
                LatLng coordinates = new LatLng(latitud, longitud);
                googleMap.addMarker(new MarkerOptions().position(coordinates).title("Detalle de Ubicacion"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, ConstInVirtual.DEFAULT_ZOOM));
                mapView.onResume();

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {
                        // TODO Auto-generated method stub
                        //lstLatLngs.add(point);
                        latitud = point.latitude;
                        longitud = point.longitude;

                        googleMap.clear();
                        googleMap.addMarker(new MarkerOptions().position(point));
                    }
                });
            }
        });
    }

    public void dispose() {
        try {
            ConstInVirtual.disposable.clear();
        } catch (Exception er) {

        }
    }
}
