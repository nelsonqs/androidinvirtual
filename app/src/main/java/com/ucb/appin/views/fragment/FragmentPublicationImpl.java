package com.ucb.appin.views.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.ucb.appin.R;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.presenters.PresenterPublication;
import com.ucb.appin.presenters.PresenterPublicationImpl;
import com.ucb.appin.presenters.adapters.PublicationsAdapter;
import com.ucb.appin.views.activity.ActivityDetailPublicationImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FragmentPublicationImpl extends Fragment implements FragmentPublication {
    private View view;
    private PresenterPublication presenterPublication;

    @BindView(R.id.l_publications)
    RecyclerView recycler;

    @BindView(R.id.list_progress)
    View mProgressView;

    List<AvisosBean> listAvisosToSarch;

    private OnFragmentInteractionListener mListener;

    PublicationsAdapter publicationsAdapter;

    public FragmentPublicationImpl() {
        // Required empty public constructor
    }

    public static FragmentPublicationImpl newInstance(String param1, String param2) {
        FragmentPublicationImpl fragment = new FragmentPublicationImpl();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_publications_view, container, false);
        this.recycler = this.view.findViewById(R.id.l_publications);
        this.mProgressView = this.view.findViewById(R.id.list_progress);
        this.presenterPublication = new PresenterPublicationImpl(this.getActivity().getApplicationContext(), this);


        this.presenterPublication.listAvisos("");

        reloadData();
        return this.view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            recycler.setVisibility(show ? View.GONE : View.VISIBLE);
            recycler.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    recycler.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            recycler.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void loadData(List<AvisosBean> listAvisos) {

        this.listAvisosToSarch = new ArrayList<AvisosBean>();
        this.listAvisosToSarch.addAll(listAvisos);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        this.publicationsAdapter = new PublicationsAdapter(this.getActivity().getApplicationContext(), listAvisos, this.presenterPublication);
        this.recycler.setLayoutManager(linearLayoutManager);
        this.recycler.setAdapter(this.publicationsAdapter);
        this.recycler.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void searchPublication(String searchText) {
        this.presenterPublication.searchListPublication(searchText);
    }

    @Override
    public void showDetailPublication(AvisosBean avisosBean) {
        Intent intent = new Intent(getActivity(), ActivityDetailPublicationImpl.class);
        Gson gson = new Gson();
        String jAvisosBean = gson.toJson(avisosBean);
        intent.putExtra("avisosBean", jAvisosBean);
        getActivity().startActivity(intent);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void finish() {

    }

    @Override
    public void reloadData() {
        this.presenterPublication.listAvisos("");
    }


}
