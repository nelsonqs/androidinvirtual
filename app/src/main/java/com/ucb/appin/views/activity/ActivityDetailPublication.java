package com.ucb.appin.views.activity;

/**
 * Created by tecnosim on 2/9/2018.
 */

public interface ActivityDetailPublication {
    void showProgress(final boolean show);
    void finish();
    void showMessage(String message);
}
