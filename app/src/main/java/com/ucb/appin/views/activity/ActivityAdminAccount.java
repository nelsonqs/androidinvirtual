package com.ucb.appin.views.activity;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.CuentasBean;

import java.util.List;

/**
 * Created by Juan choque on 2/4/2018.
 */

public interface ActivityAdminAccount {
    void loadData(CuentasBean cuentasBean);

    void showProgress(boolean b);

    void finish();

    void showMessage(String messageOkOperation);
}
