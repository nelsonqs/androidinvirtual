package com.ucb.appin.views.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.ucb.appin.R;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.presenters.PresenterMyPublication;
import com.ucb.appin.presenters.PresenterMyPublicationImpl;
import com.ucb.appin.presenters.adapters.MyPublicationsAdapter;
import com.ucb.appin.views.activity.ActivityAddMyPublicationImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FragmentMyPublicationImpl extends Fragment implements FragmentMyPublication {

    private OnFragmentInteractionListener mListener;
    private View view;
    private PresenterMyPublication presenterMyPublication;

    @BindView(R.id.l_my_publications)
    RecyclerView recycler;
    @BindView(R.id.add_progress)
    View mProgressView;

    @BindView(R.id.lin_my_publications)
    View mLinearLayout;

    @BindView(R.id.spi_l_tipo)
    Spinner spiLTipo;
    @BindView(R.id.spi_l_transaccion)
    Spinner spiLTransaccion;
    List<AvisosBean> listAvisosCopy;
    MyPublicationsAdapter myPublicationsAdapter;

    private List<TipoAvisoBean> listTipoAvisoBeans = new ArrayList<TipoAvisoBean>();
    private List<TransaccionAviso> listTransaccionAvisos = new ArrayList<TransaccionAviso>();

    private String[] slistaTipoAvisos;
    private String[] slistaTransaccionAvisos;

    public FragmentMyPublicationImpl() {
        // Required empty public constructor
    }

    public static FragmentMyPublicationImpl newInstance(String param1, String param2) {
        FragmentMyPublicationImpl fragment = new FragmentMyPublicationImpl();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_my_publications_view, container, false);

        this.recycler = this.view.findViewById(R.id.l_my_publications);
        this.mLinearLayout = this.view.findViewById(R.id.lin_my_publications);
        this.mProgressView = this.view.findViewById(R.id.list_my_progress);

        this.spiLTipo = this.view.findViewById(R.id.spi_l_tipo);
        this.spiLTransaccion = this.view.findViewById(R.id.spi_l_transaccion);

        this.spiLTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        this.presenterMyPublication = new PresenterMyPublicationImpl(this.getActivity().getApplicationContext(),this);

        this.presenterMyPublication.listTipoAvisos();
        this.presenterMyPublication.listTransaccionAvisos();

        reloadData();

        return this.view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void loadData(List<AvisosBean> listAvisos) {
        this.listAvisosCopy = new ArrayList<AvisosBean>();
        this.listAvisosCopy.addAll(listAvisos);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        myPublicationsAdapter = new MyPublicationsAdapter(this.getActivity().getApplicationContext(), listAvisos, this.presenterMyPublication);
        this.recycler.setLayoutManager(linearLayoutManager);
        this.recycler.setAdapter(myPublicationsAdapter);
        this.recycler.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void showDialogDelete(final AvisosBean avisosBean) {
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getString(R.string.confirmation))
                //.setIcon(R.mipmap.ic_clear_black_18dp)
                .setMessage(getActivity().getString(R.string.delete_messaje))
                .setCancelable(false)
                .setPositiveButton(getActivity().getString(R.string.delete), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        presenterMyPublication.deleteAviso(avisosBean);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), null)
                .show();
    }

    @Override
    public void showEditAviso(AvisosBean avisosBean){
        Intent intent = new Intent(getActivity(), ActivityAddMyPublicationImpl.class);
        Gson gson = new Gson();
        String jAvisosBean = gson.toJson(avisosBean);
        intent.putExtra("avisosBean", jAvisosBean);
        getActivity().startActivity(intent);
    }

    @Override
    public void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLinearLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            mLinearLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLinearLayout.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLinearLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void showMessage(String incorrectOperation) {

    }

    @Override
    public void finish() {

    }

    @Override
    public void reloadData() {
        this.presenterMyPublication.listAvisos("");
    }

    @Override
    public void searchAvisos(String searchText) {
        if(listAvisosCopy != null){
            List<AvisosBean>auxListAvisos = new ArrayList<AvisosBean>();
            if(!searchText.isEmpty()){
                for(int i = 0;i < listAvisosCopy.size();i++){
                    AvisosBean avisosBean = listAvisosCopy.get(i);
                    if((avisosBean.getDescripcion().toUpperCase()).contains(searchText.toUpperCase())){
                        auxListAvisos.add(avisosBean);
                    }
                }
            }
            else{
                auxListAvisos.addAll(listAvisosCopy);
            }

            this.myPublicationsAdapter.setItemsAll(auxListAvisos);
            this.myPublicationsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void loadDataTipoAviso(List<TipoAvisoBean> listTipoAvisoBeans){
        this.listTipoAvisoBeans = listTipoAvisoBeans;
        if(this.listTipoAvisoBeans.size() > 0){
            this.slistaTipoAvisos = new String[this.listTipoAvisoBeans.size()];
            for(int i = 0;i < listTipoAvisoBeans.size();i++){
                TipoAvisoBean tipoAvisoBean = listTipoAvisoBeans.get(i);
                this.slistaTipoAvisos[i] =  tipoAvisoBean.getNombre();
            }
            //seting data in spinner
            ArrayAdapter<String> adapterAvisos = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, slistaTipoAvisos);
            adapterAvisos.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spiLTipo.setAdapter(adapterAvisos);
        }
    }

    @Override
    public void loadDataTransaccionAviso(List<TransaccionAviso>listTransaccionAvisos){
        this.listTransaccionAvisos = listTransaccionAvisos;

        if(this.listTransaccionAvisos.size() > 0){
            this.slistaTransaccionAvisos = new String[this.listTransaccionAvisos.size()];
            for(int i = 0;i < listTransaccionAvisos.size();i++){
                TransaccionAviso transaccionAviso = listTransaccionAvisos.get(i);
                this.slistaTransaccionAvisos[i] =  transaccionAviso.getNombre();
            }

            //seting data in spinner
            ArrayAdapter<String> adapterTransaccion = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, slistaTransaccionAvisos);
            adapterTransaccion.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spiLTransaccion.setAdapter(adapterTransaccion);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
