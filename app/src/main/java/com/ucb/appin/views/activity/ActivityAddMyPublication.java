package com.ucb.appin.views.activity;

import com.google.android.gms.maps.GoogleMap;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;

import java.util.List;

/**
 * Created by Juan choque on 1/27/2018.
 */

public interface ActivityAddMyPublication {
    void showProgress(final boolean show);
    void showMessage(String message);
    void loadInitDataTipoAviso(List<TipoAvisoBean>listTipoAvisos);
    void loadInitDataTransaccionAviso(List<TransaccionAviso>listTransaccionAvisos);
    void onMapReady(GoogleMap var1);
    void finish();
}
