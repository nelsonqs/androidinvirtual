package com.ucb.appin.views;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;

import com.ucb.appin.R;
import com.ucb.appin.services.SynBackgroundService;
import com.ucb.appin.services.SynForegroundService;
import com.ucb.appin.util.ConstInVirtual;
import com.ucb.appin.views.activity.ActivityAddMyPublicationImpl;
import com.ucb.appin.views.activity.ActivityAdminAccountImpl;
import com.ucb.appin.views.fragment.FragmentMappViewImpl;
import com.ucb.appin.views.fragment.FragmentMyPublicationImpl;
import com.ucb.appin.views.fragment.FragmentPublicationImpl;

public class MainView extends AppCompatActivity implements FragmentMappViewImpl.OnFragmentInteractionListener,
        FragmentPublicationImpl.OnFragmentInteractionListener,
        FragmentMyPublicationImpl.OnFragmentInteractionListener {

    private SparseArray<Fragment> registeredFragments = new SparseArray<>();
    private int indexFragment = 0;

    private BottomNavigationView navigation;
    private Menu menu;
    private boolean isMultiPanel;
    private SearchView searchView;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment fragment = new FragmentPublicationImpl();
            switch (item.getItemId()) {
                case R.id.nav_publications:
                    indexFragment = ConstInVirtual.INDEX_FRAGMENT_AVISOS;
                    hideShowMenu(false);
                    fragment = new FragmentPublicationImpl();
                    break;
                case R.id.nav_maps:
                    indexFragment = ConstInVirtual.INDEX_FRAGMENT_MAPA;
                    hideShowMenu(false);
                    fragment = new FragmentMappViewImpl();
                    break;
                case R.id.nav_my_publications:
                    indexFragment = ConstInVirtual.INDEX_FRAGMENT_MY_AVISOS;
                    hideShowMenu(true);
                    fragment = new FragmentMyPublicationImpl();
                    break;
            }
            Log.v("----<>", "......" + indexFragment);
            registeredFragments.append(indexFragment, fragment);

            loadFragment(fragment);

            return false;
        }

    };

    @Override
    @SuppressLint("RestrictedApi")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //**********************TEST ONLY***************************
        SynForegroundService synForegroundService = new SynForegroundService();
        synForegroundService.syncronizedCloudLocalTipoAvisos();
        synForegroundService.syncronizedCloudLocalTransaccionAvisos();
        //**********************FIN TEST****************************

        //init test only
        Fragment rFragment = new FragmentPublicationImpl();
        this.loadFragment(rFragment);
        registeredFragments.append(indexFragment, rFragment);
        //////////////////////////////////////////////////////////////////////////////////////////////////

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        navigation.setItemIconTintList(null);
        setMultiPanel();

        //esta es otra de las
        Intent intent = new Intent(this, SynBackgroundService.class);
        startService(intent);

    }

    public void loadFragment(Fragment fragment) {

        try {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_fragment, fragment);
            fragmentTransaction.commit();
        } catch (Exception ex) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //for search
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String searchText) {
                searchMisAvisos(searchText);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        //end for search

        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_add) {
            Intent intent = new Intent(MainView.this, ActivityAddMyPublicationImpl.class);
            int requestCode = 1; // Or some number you choose
            startActivityForResult(intent, requestCode);
        }
        if (id == R.id.menu_cuenta) {
            Intent intent = new Intent(MainView.this, ActivityAdminAccountImpl.class);
            int requestCode = 2; // Or some number you choose
            startActivityForResult(intent, requestCode);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.v("----->",">>>>>>....................>>>>>>>" + indexFragment);
        Fragment rFragment = registeredFragments.get(indexFragment);
        if (rFragment == null) {
            rFragment = new FragmentPublicationImpl();
            indexFragment = 0;
        } else {
            if (indexFragment == ConstInVirtual.INDEX_FRAGMENT_MY_AVISOS) {
                FragmentMyPublicationImpl fragmentMyPublicationImpl = (FragmentMyPublicationImpl) rFragment;
                fragmentMyPublicationImpl.reloadData();
            }
            if (indexFragment == ConstInVirtual.INDEX_FRAGMENT_AVISOS) {
                FragmentPublicationImpl fragmentpublication = (FragmentPublicationImpl) rFragment;
                fragmentpublication.reloadData();
            }
        }

    }

//    @Override
//    public void onListClick(AvisosBean avisosBean) {
//        Intent intent = new Intent(this, ActivityPublicationDetailImpl.class);
//        intent.putExtra("subject", avisosBean.getTitulo());
//        intent.putExtra("message", avisosBean.getDescripcion());
//        intent.putExtra("senderName", avisosBean.getDireccion());
//        startActivity(intent);
//    }

    private void setMultiPanel() {
        isMultiPanel = (getSupportFragmentManager().findFragmentById(R.id.fragmentDetailsMail) != null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    public void hideShowMenu(boolean visible) {
        MenuItem menuItem = menu.findItem(R.id.menu_add);
        menuItem.setVisible(visible);
    }

    private void searchMisAvisos(String searchText) {
        try {
            if(indexFragment == ConstInVirtual.INDEX_FRAGMENT_MY_AVISOS){
                FragmentMyPublicationImpl fragmentMyPublicationImpl = (FragmentMyPublicationImpl)this.registeredFragments.get(ConstInVirtual.INDEX_FRAGMENT_MY_AVISOS);
                if(fragmentMyPublicationImpl != null){
                    fragmentMyPublicationImpl.searchAvisos(searchText);
                }
            }else if(indexFragment == ConstInVirtual.INDEX_FRAGMENT_AVISOS){

                FragmentPublicationImpl fragmentPublicationImpl = (FragmentPublicationImpl)this.registeredFragments.get(ConstInVirtual.INDEX_FRAGMENT_AVISOS);
                if(fragmentPublicationImpl != null){
                    fragmentPublicationImpl.searchPublication(searchText);
                }
            }
        }catch (Exception err){
        }
    }

}
