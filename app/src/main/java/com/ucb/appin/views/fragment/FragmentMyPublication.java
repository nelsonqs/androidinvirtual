package com.ucb.appin.views.fragment;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;

import java.util.List;

/**
 * Created by Juan choque on 1/27/2018.
 */

public interface FragmentMyPublication {
    void loadData(List<AvisosBean> listAvisos);
    void showDialogDelete(AvisosBean avisosBean);
    void showEditAviso(AvisosBean avisosBean);

    void showProgress(boolean b);

    void showMessage(String incorrectOperation);

    void finish();

    void reloadData();

    void searchAvisos(String searchText);

    void loadDataTipoAviso(List<TipoAvisoBean> listTipoAvisoBeans);

    void loadDataTransaccionAviso(List<TransaccionAviso>listTransaccionAvisos);
}
