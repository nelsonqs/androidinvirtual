package com.ucb.appin.services.localServices;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Juan choque on 2/12/2018.
 */

public interface SynLocalService {
    List<AvisosBean>listAvisos(List<AvisosBean>listAvisos);

    int getMaxId();

    void insertAviso(AvisosBean avisosBean);

    void updateAviso(AvisosBean avisosBean);

    List<BitacoraBean> listBitacora();

    AvisosBean getAvisoIdc(String idBean);

    AvisosBean getAvisoById(int id);

    void removeBitacoraById(int id);

    TipoAvisoBean getTipoAvisoByIdc(Integer id);

    void insertTipoAviso(TipoAvisoBean cTipoAvisoBean);

    void updateTipoAviso(TipoAvisoBean lTipoAvisoBean);

    TransaccionAviso getTransaccionAvisoByIdc(Integer id);

    void insertTransaccionAviso(TransaccionAviso cTransaccionAvisoBean);

    void updateTransaccionAviso(TransaccionAviso lTransaccionAvisoBean);

    void insertCuentas(CuentasBean cuentasBean);

    void updateCuentas(CuentasBean cuentasBean);

    void insertBitacora(BitacoraBean bitacoraBean);

    BitacoraBean getBitacoraByIdBean(int id);

    CuentasBean getCuenta();
}
