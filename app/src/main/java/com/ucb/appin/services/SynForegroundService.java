package com.ucb.appin.services;

import android.os.AsyncTask;
import android.util.Log;

import com.ucb.appin.data.api.client.InMobService;
import com.ucb.appin.data.api.client.InMobServiceImpl;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.presenters.PresenterMyAccount;
import com.ucb.appin.services.localServices.SynLocalService;
import com.ucb.appin.services.localServices.SynLocalServiceImpl;
import com.ucb.appin.util.ConstInVirtual;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

/**
 * Created by Juan choque on 2/12/2018.
 */

public class SynForegroundService {
    private InMobService inMobService;

    private TipoAvisoTask tipoAvisoTask;
    private TransaccionAvisoTask transaccionAvisoTask;
    private CuentasTask cuentasTask;

    public SynForegroundService() {
        this.inMobService = new InMobServiceImpl();
    }

    //***************************************************************************************************
    public void syncronizedCloudLocalTipoAvisos(){
        io.reactivex.Observable<List<TipoAvisoBean>> observable = this.inMobService.listTipoAvisos();

        Disposable disposable = observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        data -> {
                            syncronizedLocalTipoAviso(data);
                            Log.v("......>",">RETURN TIPO AVISO>" + data.size());
                        },
                        error -> {
                            syncronizedLocalTipoAviso(null);
                            Log.v("......>",">ERROR RETURN TIPO AVISO>" + error.getMessage());
                            error.printStackTrace();
                        }
                );
    }

    private void syncronizedLocalTipoAviso(List<TipoAvisoBean> data) {
        tipoAvisoTask = new TipoAvisoTask(data);
        tipoAvisoTask.execute((Void) null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class TipoAvisoTask extends AsyncTask<Void, Void, Boolean> {
        private List<TipoAvisoBean>listTipoAvisos;
        private SynLocalService synLocalService;

        public TipoAvisoTask(List<TipoAvisoBean> listTipoAvisos) {
            this.listTipoAvisos = listTipoAvisos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            this.synLocalService = new SynLocalServiceImpl();

            boolean result = false;
            TipoAvisoBean cTipoAvisoBean = null;
            TipoAvisoBean lTipoAvisoBean = null;

            try {
                for (int i = 0;i < listTipoAvisos.size();i++){
                    cTipoAvisoBean = listTipoAvisos.get(i);
                    lTipoAvisoBean = this.synLocalService.getTipoAvisoByIdc(cTipoAvisoBean.getId());
                    if(lTipoAvisoBean == null){
                        cTipoAvisoBean.setIdc(cTipoAvisoBean.getId());
                        this.synLocalService.insertTipoAviso(cTipoAvisoBean);
                    }
                    else {
                        lTipoAvisoBean.setNombre(cTipoAvisoBean.getNombre());
                        lTipoAvisoBean.setEstado(cTipoAvisoBean.getEstado());

                        this.synLocalService.updateTipoAviso(lTipoAvisoBean);
                    }
                }

                result = true;

            }catch (Exception er){
                result = false;
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            tipoAvisoTask = null;
        }

        @Override
        protected void onCancelled() {
            tipoAvisoTask = null;
        }
    }


    //***************************************************************************************************
    public void syncronizedCloudLocalTransaccionAvisos(){
        io.reactivex.Observable<List<TransaccionAviso>> observable = this.inMobService.listTransaccionAvisos();

        Disposable disposable = observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        data -> {
                            syncronizedLocalTransaccionAviso(data);
                            Log.v("......>",">RETURN TRAN AVISO>" + data.size());
                        },
                        error -> {
                            syncronizedLocalTransaccionAviso(null);
                            Log.v("......>",">ERROR RETURN TRAN AVISO>" + error.getMessage());
                            error.printStackTrace();
                        }
                );
    }

    private void syncronizedLocalTransaccionAviso(List<TransaccionAviso> data) {
        transaccionAvisoTask = new TransaccionAvisoTask(data);
        transaccionAvisoTask.execute((Void) null);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class TransaccionAvisoTask extends AsyncTask<Void, Void, Boolean> {
        private List<TransaccionAviso>listTransaccionAvisos;
        private SynLocalService synLocalService;

        public TransaccionAvisoTask(List<TransaccionAviso> listTransaccionAvisos) {
            this.listTransaccionAvisos = listTransaccionAvisos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            this.synLocalService = new SynLocalServiceImpl();

            boolean result = false;
            TransaccionAviso cTransaccionAvisoBean = null;
            TransaccionAviso lTransaccionAvisoBean = null;

            try {
                for (int i = 0;i < listTransaccionAvisos.size();i++){
                    cTransaccionAvisoBean = listTransaccionAvisos.get(i);
                    lTransaccionAvisoBean = this.synLocalService.getTransaccionAvisoByIdc(cTransaccionAvisoBean.getId());
                    if(lTransaccionAvisoBean == null){
                        cTransaccionAvisoBean.setIdc(cTransaccionAvisoBean.getId());
                        this.synLocalService.insertTransaccionAviso(cTransaccionAvisoBean);
                    }
                    else {
                        lTransaccionAvisoBean.setNombre(cTransaccionAvisoBean.getNombre());
                        lTransaccionAvisoBean.setEstado(cTransaccionAvisoBean.getEstado());

                        this.synLocalService.updateTransaccionAviso(lTransaccionAvisoBean);
                    }
                }

                result = true;

            }catch (Exception er){
                result = false;
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            tipoAvisoTask = null;
        }

        @Override
        protected void onCancelled() {
            tipoAvisoTask = null;
        }
    }




    //***************************************************************************************************
    public void syncronizedCloudLocalSetCuentas(CuentasBean cuentasBean, PresenterMyAccount presenterMyAccount){

        io.reactivex.Observable<CuentasBean> observable = this.inMobService.setCuentas(cuentasBean);

        Disposable disposable = observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        data -> {
                            syncronizedLocalSetCuentas(data, presenterMyAccount);
                            Log.v("--->",">RETURN CORRECT ACCOUNT>");
                        },
                        error -> {
                            syncronizedLocalSetCuentas(null, presenterMyAccount);
                            Log.v("--->",">RETURN ERROR ACCOUNT>");
                            error.printStackTrace();
                        }
                );
    }

    private void syncronizedLocalSetCuentas(CuentasBean data, PresenterMyAccount presenterMyAccount) {
        cuentasTask = new CuentasTask(data, presenterMyAccount);
        cuentasTask.execute((Void) null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class CuentasTask extends AsyncTask<Void, Void, Boolean> {
        private CuentasBean cuentasBean;
        private SynLocalService synLocalService;
        private PresenterMyAccount presenterMyAccount;

        public CuentasTask(CuentasBean cuentasBean, PresenterMyAccount presenterMyAccount) {
            this.cuentasBean = cuentasBean;
            this.presenterMyAccount = presenterMyAccount;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            this.synLocalService = new SynLocalServiceImpl();

            boolean result = false;

            try {
                if(cuentasBean != null){
                    this.synLocalService.insertCuentas(cuentasBean);
                }

                result = true;

            }catch (Exception er){
                result = false;
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            tipoAvisoTask = null;
            if(success){
                presenterMyAccount.showMessage(ConstInVirtual.MESSAGE_OK_OPERATION);
            }
            else{
                presenterMyAccount.showMessage(ConstInVirtual.MESSAGE_ERROR_OPERATION);
            }

            presenterMyAccount.showProgress(false);
        }

        @Override
        protected void onCancelled() {
            tipoAvisoTask = null;
        }
    }

}
