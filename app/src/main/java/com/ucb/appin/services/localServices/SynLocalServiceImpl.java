package com.ucb.appin.services.localServices;

import android.content.Context;
import android.util.Log;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.util.UtilInmo;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Juan choque on 2/12/2018.
 */

public class SynLocalServiceImpl implements SynLocalService {
    private Context context;

    public SynLocalServiceImpl(Context context) {
        this.context = context;
    }

    public SynLocalServiceImpl() {
    }

    @Override
    public List<AvisosBean> listAvisos(List<AvisosBean>listAvisos) {
        Realm realm = Realm.getDefaultInstance();
        List<AvisosBean>result = new ArrayList<AvisosBean>();
        try {
            RealmResults<AvisosBean> avisos = realm.where(AvisosBean.class)
                    //.equalTo("estado", ConstantesDcompras.ACTIVO)
                    .findAllSorted("id", Sort.DESCENDING);
            if(avisos != null){
                for(AvisosBean avisosBean: avisos){
                    result.add(avisosBean);
                }
            }
        }catch (Exception err){
            result = new ArrayList<AvisosBean>();
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }

        return result;
    }

    @Override
    public void insertAviso(AvisosBean avisosBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(avisosBean != null){
                int id = getMaxId();

                avisosBean.setId((id + 1));

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(avisosBean);
                realm.commitTransaction();

                realm.refresh();
            }
        }catch (Exception er){
            Log.i("---ERROR INSERT A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public void updateAviso(AvisosBean avisosBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(avisosBean != null){
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(avisosBean);
                realm.commitTransaction();

                realm.refresh();
            }
        }catch (Exception er){
            Log.i("---ERROR UPDATE A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public int getMaxId() {
        Realm realm = Realm.getDefaultInstance();
        int result = 0;
        try {
            Number aux = realm.where(AvisosBean.class)
                    .max("id");
            if(aux != null){
                result = aux.intValue();
            }
        }catch (Exception er){
            result = 0;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    private int getMaxIdTAviso() {
        Realm realm = Realm.getDefaultInstance();
        int result = 0;
        try {
            Number aux = realm.where(TipoAvisoBean.class)
                    .max("id");
            if(aux != null){
                result = aux.intValue();
            }
        }catch (Exception er){
            result = 0;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    private int getMaxIdTrAviso() {
        Realm realm = Realm.getDefaultInstance();
        int result = 0;
        try {
            Number aux = realm.where(TransaccionAviso.class)
                    .max("id");
            if(aux != null){
                result = aux.intValue();
            }
        }catch (Exception er){
            result = 0;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    private int getMaxIdCuenta() {
        Realm realm = Realm.getDefaultInstance();
        int result = 0;
        try {
            Number aux = realm.where(CuentasBean.class)
                    .max("id");
            if(aux != null){
                result = aux.intValue();
            }
        }catch (Exception er){
            result = 0;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    @Override
    public List<BitacoraBean> listBitacora() {
        Realm realm = Realm.getDefaultInstance();
        List<BitacoraBean>result = new ArrayList<BitacoraBean>();
        try {
            RealmResults<BitacoraBean> bitacoras = realm.where(BitacoraBean.class)
                    .equalTo("estado","A")
                    .findAll();

            if(bitacoras != null){
                for (BitacoraBean bitacoraBean : bitacoras){
                    Log.v("------>",bitacoraBean.getId() + ">>" + bitacoraBean.getEstado());
                    BitacoraBean nbitacoraBean = new BitacoraBean();
                    nbitacoraBean.setId(bitacoraBean.getId());
                    nbitacoraBean.setIdc(bitacoraBean.getIdc());
                    nbitacoraBean.setIdBean(bitacoraBean.getIdBean());
                    nbitacoraBean.setOperacion(bitacoraBean.getOperacion());
                    nbitacoraBean.setTelefono(bitacoraBean.getTelefono());
                    result.add(nbitacoraBean);
                }
            }
            Log.v(">>>>>>", ">>EN DB BI>>" + bitacoras);
        }catch (Exception err){
            result = new ArrayList<BitacoraBean>();
            Log.v(">>>>>>", ">>ERROR EN DB BI>>" + err.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }

        return result;
    }

    @Override
    public AvisosBean getAvisoIdc(String id) {
        Realm realm = Realm.getDefaultInstance();
        AvisosBean result = null;
        try {
            AvisosBean avisosBean = realm.where(AvisosBean.class)
                    .equalTo("idc", id)
                    .findFirst();

            if(avisosBean != null){
                result = new AvisosBean();

                result.setId(avisosBean.getId());
                result.setEstado(avisosBean.getEstado());
                result.setTitulo(avisosBean.getTitulo());
                result.setDescripcion(avisosBean.getDescripcion());
                result.setDireccion(avisosBean.getDireccion());
                result.setPrecio(avisosBean.getPrecio());
                result.setIdc(avisosBean.getIdc());
                result.setLatitud(avisosBean.getLatitud());
                result.setLongitud(avisosBean.getLongitud());
                result.setImagen(avisosBean.getImagen());
                result.setTipoaviso(avisosBean.getTipoaviso());
                result.setTransaccionAviso(avisosBean.getTransaccionaviso());
                result.setTelefono(avisosBean.getTelefono());
                result.setCuenta(avisosBean.getCuenta());
                result.setOrden(avisosBean.getOrden());
            }

        }catch (Exception err){
            result = null;
            err.printStackTrace();
            Log.v(">>>>",">ERROR EN DB getidc>" + err.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    @Override
    public AvisosBean getAvisoById(int id) {
        Realm realm = Realm.getDefaultInstance();
        AvisosBean result = null;
        try {
            AvisosBean avisosBean = realm.where(AvisosBean.class)
                    .equalTo("id", id)
                    .findFirst();

            if(avisosBean != null) {
                result = new AvisosBean();

                result.setId(avisosBean.getId());
                result.setEstado(avisosBean.getEstado());
                result.setTitulo(avisosBean.getTitulo());
                result.setDescripcion(avisosBean.getDescripcion());
                result.setDireccion(avisosBean.getDireccion());
                result.setPrecio(avisosBean.getPrecio());
                result.setIdc(avisosBean.getIdc());
                result.setLatitud(avisosBean.getLatitud());
                result.setLongitud(avisosBean.getLongitud());
                result.setImagen(avisosBean.getImagen());
                result.setTipoaviso(avisosBean.getTipoaviso());
                result.setTransaccionAviso(avisosBean.getTransaccionaviso());
                result.setTelefono(avisosBean.getTelefono());
                result.setCuenta(avisosBean.getCuenta());
                result.setOrden(avisosBean.getOrden());
            }

        }catch (Exception err){
            Log.v(".....>",">error al recupera datos by id>" + err.getMessage());
            result = null;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }


    public BitacoraBean getBitacoraById(int id) {
        Realm realm = Realm.getDefaultInstance();
        BitacoraBean result = null;
        try {
            BitacoraBean bitacoraBean = realm.where(BitacoraBean.class)
                    .equalTo("id", id)
                    .findFirst();

            if(bitacoraBean != null){
                result = new BitacoraBean();

                result.setId(bitacoraBean.getId());
                result.setIdBean(bitacoraBean.getIdBean());
                result.setOperacion(bitacoraBean.getOperacion());
                result.setIdc(bitacoraBean.getIdc());
                result.setTelefono(bitacoraBean.getTelefono());
            }

        }catch (Exception err){
            result = null;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    @Override
    public void removeBitacoraById(int id) {
        Realm lrealm = Realm.getDefaultInstance();
        try{
            BitacoraBean bitacoraBean = getBitacoraById(id);
            /*BitacoraBean bitacoraBean = lrealm.where(BitacoraBean.class)
                    .equalTo("id", id)
                    .findFirst();*/
            Log.v("....>",">ID FOR DELETE>" + id);
            Log.v("....>",">DELETE DB>" + bitacoraBean);

            if(bitacoraBean != null){
                lrealm.beginTransaction();
                bitacoraBean.setEstado("X");
                lrealm.copyToRealmOrUpdate(bitacoraBean);
                lrealm.commitTransaction();

                lrealm.refresh();
            }
        }catch (Exception err){
            Log.v("....>", ">ERROR EN BITA TO DELETE>" + err.getMessage());
        }
        finally {
            if(lrealm != null){
                lrealm.close();
            }
        }
    }

    @Override
    public TipoAvisoBean getTipoAvisoByIdc(Integer id) {
        Realm realm = Realm.getDefaultInstance();
        TipoAvisoBean result = null;
        try {
            TipoAvisoBean tipoAvisoBean = realm.where(TipoAvisoBean.class)
                    .equalTo("idc", id)
                    .findFirst();

            if(tipoAvisoBean != null){
                result = new TipoAvisoBean();
                result.setId(tipoAvisoBean.getId());
                result.setIdc(tipoAvisoBean.getIdc());
                result.setNombre(tipoAvisoBean.getNombre());
                result.setEstado(tipoAvisoBean.getEstado());
            }
        }catch (Exception err){
            result = null;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    @Override
    public void insertTipoAviso(TipoAvisoBean tipoAvisoBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(tipoAvisoBean != null){
                int id = getMaxIdTAviso();

                tipoAvisoBean.setId((id + 1));

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(tipoAvisoBean);
                realm.commitTransaction();
            }
        }catch (Exception er){
            Log.i("---ERROR INSERT A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public void updateTipoAviso(TipoAvisoBean tipoAvisoBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(tipoAvisoBean != null){
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(tipoAvisoBean);
                realm.commitTransaction();
            }
        }catch (Exception er){
            Log.i("---ERROR UPDATE A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public TransaccionAviso getTransaccionAvisoByIdc(Integer id) {
        Realm realm = Realm.getDefaultInstance();
        TransaccionAviso result = null;
        try {
            TransaccionAviso transaccionAviso = realm.where(TransaccionAviso.class)
                    .equalTo("idc", id)
                    .findFirst();

            if(transaccionAviso != null){
                result = new TransaccionAviso();
                result.setId(transaccionAviso.getId());
                result.setIdc(transaccionAviso.getIdc());
                result.setNombre(transaccionAviso.getNombre());
                result.setEstado(transaccionAviso.getEstado());
            }

        }catch (Exception err){
            result = null;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    @Override
    public void insertTransaccionAviso(TransaccionAviso transaccionAviso) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(transaccionAviso != null){
                int id = getMaxIdTrAviso();

                transaccionAviso.setId((id + 1));

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(transaccionAviso);
                realm.commitTransaction();
            }
        }catch (Exception er){
            Log.i("---ERROR INSERT A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public void updateTransaccionAviso(TransaccionAviso transaccionAviso) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(transaccionAviso != null){
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(transaccionAviso);
                realm.commitTransaction();
            }
        }catch (Exception er){
            Log.i("---ERROR UPDATE A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public void insertCuentas(CuentasBean cuentasBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(cuentasBean != null){
                Log.v("&&&&&&&>",">>" + cuentasBean.getAparteno());
                Log.v("&&&&&&&>",">>" + cuentasBean.getAmaterno());
                Log.v("&&&&&&&>",">>" + cuentasBean.getNombres());
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(cuentasBean);
                realm.commitTransaction();

                Log.i("------>",">CREA CUENT CORRECTAMENTE>");
            }
        }catch (Exception er){
            Log.i("---ERROR INSERT A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public CuentasBean getCuenta(){
        UtilInmo utilInmo = new UtilInmo(context);
        String imei = utilInmo.getNumberPhone();
        Log.v("&&&&&&>",">>" + imei);
        Realm realm = Realm.getDefaultInstance();
        CuentasBean result = null;
        try {
            CuentasBean cuentasBean = realm.where(CuentasBean.class)
                    .equalTo("celular", imei)
                    .findFirst();

            if(cuentasBean != null){
                result = new CuentasBean();
                result.setCelular(cuentasBean.getCelular());
                result.setNombres(cuentasBean.getNombres());
                result.setAparteno(cuentasBean.getAparteno());
                result.setAmaterno(cuentasBean.getAmaterno());
                result.setId(cuentasBean.getId());
            }

        }catch (Exception err){
            result = null;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

    @Override
    public void updateCuentas(CuentasBean cuentasBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            if(cuentasBean != null){
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(cuentasBean);
                realm.commitTransaction();
            }
        }catch (Exception er){
            Log.i("---ERROR UPDATE A--->",">>" + er.getMessage());
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public void insertBitacora(BitacoraBean bitacoraBean) {
        Realm realm = Realm.getDefaultInstance();
        try {
            realm.beginTransaction();
            bitacoraBean.setEstado("A");
            realm.copyToRealmOrUpdate(bitacoraBean);
            realm.commitTransaction();

            realm.refresh();
        }catch (Exception err){

        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
    }

    @Override
    public BitacoraBean getBitacoraByIdBean(int idBean) {
        Realm realm = Realm.getDefaultInstance();
        BitacoraBean result = null;
        try {
            BitacoraBean bitacoraBean = realm.where(BitacoraBean.class)
                    .equalTo("idBean", idBean)
                    .findFirst();

            if(bitacoraBean != null){
                result = new BitacoraBean();
                result.setId(bitacoraBean.getId());
                result.setIdc(bitacoraBean.getIdc());
                result.setIdBean(bitacoraBean.getIdBean());
                result.setOperacion(bitacoraBean.getOperacion());
                result.setEstado(bitacoraBean.getEstado());
            }

        }catch (Exception err){
            result = null;
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }
        return result;
    }

}
