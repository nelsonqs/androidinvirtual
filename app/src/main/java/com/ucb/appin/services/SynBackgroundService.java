package com.ucb.appin.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ucb.appin.R;
import com.ucb.appin.data.api.client.InMobService;
import com.ucb.appin.data.api.client.InMobServiceImpl;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.services.localServices.SynLocalService;
import com.ucb.appin.services.localServices.SynLocalServiceImpl;
import com.ucb.appin.util.ConstInVirtual;
import com.ucb.appin.util.UtilInmo;
import com.ucb.appin.views.MainView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

/**
 * Created by Juan choque on 2/10/2018.
 */

public class SynBackgroundService extends Service {
    private static final String TAG = SynBackgroundService.class.getSimpleName();
    public static final int NOTIFICATION_ID = 234;

    private CloudLocalTask cloudLocalTask = null;
    private LocalTask localTask = null;
    private LocalUpdateBitacoraTask localUpdateBitacoraTask = null;
    private WaitTask waitTask = null;

    private InMobService inMobService;

    public SynBackgroundService() {
        this.inMobService = new InMobServiceImpl();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "Iniciando servio",Toast.LENGTH_SHORT).show();

        loadLocalAvisos();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Deteniendo servio", Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return !componentInfo.getPackageName().equals(myPackage);
    }

    public void createNotification(String message){

        Intent intent = new Intent(this, MainView.class);

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
        taskStackBuilder.addParentStack(MainView.class);
        taskStackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = taskStackBuilder.
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);

    }


    //**********************************************************************************************
    public void loadLocalAvisos(){
        localTask = new LocalTask(this);
        localTask.execute((Void) null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    public class LocalTask extends AsyncTask<Void, Void, Boolean> {
        private List<AvisosBean>listAvisos;
        private Context context;

        public LocalTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Toast.makeText(getApplicationContext(), "Bitacora antes",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            SynLocalService synLocalService = new SynLocalServiceImpl(context);

            boolean result = false;
            try {
                listAvisos = new ArrayList<AvisosBean>();
                List<BitacoraBean> listBitacora = synLocalService.listBitacora();
                Log.v(".....>",">>cantidd de datos en en bitacora local>" + listBitacora.size());
                for (int i = 0;i < listBitacora.size();i++){
                    BitacoraBean bitacoraBean = listBitacora.get(i);
                    AvisosBean avisosBean = synLocalService.getAvisoById(bitacoraBean.getIdBean());
                    avisosBean.setIdBitacora(bitacoraBean.getId());
                    avisosBean.setOperacion(bitacoraBean.getOperacion());

                    listAvisos.add(avisosBean);
                }
                Log.v(".....>",">>cantidad datos avisos desde bitacora>" + listAvisos.size());

                if(listAvisos.size() > 0){
                    result = true;
                }
                else{
                    result = false;
                }
            }catch (Exception er){
                Log.v("......>",">>error al recuperar datos de bitacora>" + er.getMessage());
                result = false;
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            localTask = null;
            if (success) {
                syncronizedLocalCloudAvisos(listAvisos);
            }
            else{
                syncronizedCloudLocalAvisos();
            }
        }

        @Override
        protected void onCancelled() {
            localTask = null;
        }
    }

    public void syncronizedLocalCloudAvisos(List<AvisosBean>listAvisos){

        io.reactivex.Observable<List<BitacoraBean>> observable = this.inMobService.setAvisos(listAvisos);

        Disposable disposable = observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        data -> {
                            syncronizedLocalBitacora(data);
                        },
                        error -> {
                            syncronizedLocalBitacora(null);
                        }
                );
    }
    public void syncronizedLocalBitacora(List<BitacoraBean>listBitacora){
        localUpdateBitacoraTask = new LocalUpdateBitacoraTask(this, listBitacora);
        localUpdateBitacoraTask.execute((Void) null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    public class LocalUpdateBitacoraTask extends AsyncTask<Void, Void, Boolean> {
        private List<BitacoraBean>listBitacora;
        private Context context;

        public LocalUpdateBitacoraTask(Context context, List<BitacoraBean> listBitacora) {
            this.context = context;
            this.listBitacora = listBitacora;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            SynLocalService synLocalService = new SynLocalServiceImpl(context);

            boolean result = false;
            try {
                Log.v("--01-->",">RETURN DATA BITA>" + listBitacora.size());
                for(int i = 0;i < listBitacora.size();i++){
                    BitacoraBean bitacoraBean = listBitacora.get(i);
                    //update aviso in list
                    AvisosBean avisosBean = synLocalService.getAvisoById(bitacoraBean.getIdBean());
                    if(avisosBean != null){
                        avisosBean.setIdc(bitacoraBean.getIdc() + "");
                        synLocalService.updateAviso(avisosBean);
                    }
                    //remove in bitacora
                    synLocalService.removeBitacoraById(bitacoraBean.getId());
                }
                result = true;
            }catch (Exception er){

                result = false;
            }

            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            localTask = null;
            syncronizedCloudLocalAvisos();
        }

        @Override
        protected void onCancelled() {
            localTask = null;
        }
    }


    //***************************************************************************************************
    public void syncronizedCloudLocalAvisos(){
        UtilInmo utilInmo = new UtilInmo(this);
        String imei = utilInmo.getNumberPhone();

        BitacoraBean bitacoraBean = new BitacoraBean();
        bitacoraBean.setTelefono(imei);

        io.reactivex.Observable<List<AvisosBean>> observable = this.inMobService.listAvisos(bitacoraBean);

        Disposable disposable = observable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                    data -> {
                        syncronizedLocalAviso(data);
                    },
                    error -> {
                        syncronizedLocalAviso(null);
                    }
            );
    }
    public void syncronizedLocalAviso(List<AvisosBean>listAvisos){
        cloudLocalTask = new CloudLocalTask(this, listAvisos);
        cloudLocalTask.execute((Void) null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    public class CloudLocalTask extends AsyncTask<Void, Void, Boolean> {
        private List<AvisosBean>listAvisos;
        private Context context;
        private String messageNotif  = "";

        public CloudLocalTask(Context context, List<AvisosBean>listAvisos){
            this.context = context;
            this.listAvisos = listAvisos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Toast.makeText(getApplicationContext(), "Avisos antes",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            SynLocalService synLocalService = new SynLocalServiceImpl(context);

            boolean rresult = false;

            try {
                if(listAvisos != null){
                    for(int i = 0;i < listAvisos.size();i++){
                        AvisosBean cAvisosBean = listAvisos.get(i);

                        AvisosBean lAvisosBean = synLocalService.getAvisoIdc(cAvisosBean.getId() + "");

                        int idc = cAvisosBean.getId();
                        boolean operationEdit = false;
                        messageNotif = cAvisosBean.getTitulo();
                        if(lAvisosBean == null){

                            CuentasBean cuentasBean = synLocalService.getCuenta();
                            if(cuentasBean != null){
                                if(cAvisosBean.getCuenta() == cuentasBean.getId()){
                                    operationEdit = true;
                                }
                            }
                        }
                        else{
                            operationEdit = true;
                        }

                        if(operationEdit){
                            Log.v("...........",">por modificar>" + lAvisosBean);

                            if(lAvisosBean != null){
                                lAvisosBean.setTelefono(cAvisosBean.getTelefono());
                                lAvisosBean.setTransaccionAviso(cAvisosBean.getTransaccionaviso());
                                lAvisosBean.setTipoaviso(cAvisosBean.getTipoaviso());
                                lAvisosBean.setImagen(cAvisosBean.getImagen());
                                lAvisosBean.setLongitud(cAvisosBean.getLongitud());
                                lAvisosBean.setLatitud(cAvisosBean.getLatitud());
                                lAvisosBean.setDireccion(cAvisosBean.getDireccion());
                                lAvisosBean.setPrecio(cAvisosBean.getPrecio());
                                lAvisosBean.setDireccion(cAvisosBean.getDireccion());
                                lAvisosBean.setDescripcion(cAvisosBean.getDescripcion());
                                lAvisosBean.setTitulo(cAvisosBean.getTitulo());
                                lAvisosBean.setEstado(cAvisosBean.getEstado());
                                lAvisosBean.setIdc(idc + "");

                                lAvisosBean.setTipoaviso(cAvisosBean.getTipoaviso());
                                lAvisosBean.setTransaccionAviso(cAvisosBean.getTransaccionaviso());

                                synLocalService.updateAviso(lAvisosBean);
                            }

                        }
                        else{
                            Log.v("...........",">por nuevo>" + lAvisosBean);
                            cAvisosBean.setIdc(idc + "");
                            synLocalService.insertAviso(cAvisosBean);
                        }

                    }

                    rresult = true;
                }

            }catch (Exception er){
                rresult = false;
            }

            return rresult;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            cloudLocalTask = null;
            //Toast.makeText(getApplicationContext(), "Avisos despues>",Toast.LENGTH_SHORT).show();
            if (success) {
                if(isForeground(ConstInVirtual.NAME_PACKAGE) && !messageNotif.isEmpty()){
                    createNotification(messageNotif);
                }
            }

            wdelay();
        }

        @Override
        protected void onCancelled() {
            cloudLocalTask = null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    public void wdelay(){
        waitTask = new WaitTask();
        waitTask.execute((Void) null);
    }

    public class WaitTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                Thread.sleep(ConstInVirtual.WAIT_FIVE_SECONDS);
            }catch (Exception er){
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            waitTask = null;
            loadLocalAvisos();
        }

        @Override
        protected void onCancelled() {
            waitTask = null;
        }
    }

}
