package com.ucb.appin.data.api.client;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tecnosim on 1/18/2018.
 */

public interface InMobService {

    Observable<List<AvisosBean>> listAvisos(BitacoraBean bitacoraBean);

    Observable<List<BitacoraBean>> setAvisos(List<AvisosBean> listAvisos);

    Observable<List<TipoAvisoBean>> listTipoAvisos();

    Observable<List<TransaccionAviso>> listTransaccionAvisos();

    Observable<CuentasBean> setCuentas(CuentasBean cuentasBean);

    Observable<CuentasBean> updateCuentas(CuentasBean cuentasBean);
}
