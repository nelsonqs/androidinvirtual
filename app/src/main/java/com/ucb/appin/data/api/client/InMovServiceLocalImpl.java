package com.ucb.appin.data.api.client;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;

/**
 * Created by tecnosim on 1/29/2018.
 */

public class InMovServiceLocalImpl implements InMobService {
    @Override
    public Observable<List<AvisosBean>> listAvisos(BitacoraBean bitacoraBean) {
        Realm realm = Realm.getDefaultInstance();
        List<AvisosBean> list =   AvisosBean.listAllAdvices(realm,"");
        if (list.size() ==0 ){
            AvisosBean movies = new AvisosBean();
            movies.setTitulo("No Hay Datos");
            movies.setDescripcion("No hay datos");
            movies.setDireccion("No hay datos");
            movies.setTelefono("79846544");
            movies.setTipoaviso(1);
            movies.setTransaccionAviso(1);
            movies.setFecPublicacion(new Date());
            movies.setLatitud(Double.parseDouble("234"));
            movies.setLongitud(Double.parseDouble("234"));
            movies.setPrecio(Double.parseDouble("234"));
            list = new ArrayList<>();
            list.add(movies);
        }
        return Observable.fromArray(list);
    }

    @Override
    public Observable<List<BitacoraBean>> setAvisos(List<AvisosBean> listAvisos) {
        //Actualmente no esta implementado, ya va !!!.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Observable<List<TipoAvisoBean>> listTipoAvisos() {
        //Actualmente no esta implementado, ya va !!!.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Observable<List<TransaccionAviso>> listTransaccionAvisos() {
        //Actualmente no esta implementado, ya va !!!.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Observable<CuentasBean> setCuentas(CuentasBean cuentasBean) {
        //Actualmente no esta implementado, ya va !!!.
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Observable<CuentasBean> updateCuentas(CuentasBean cuentasBean) {
        //Actualmente no esta implementado, ya va !!!.
        throw new UnsupportedOperationException("Unsupported operation");
    }
}
