package com.ucb.appin.data.networking;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.util.ConstInVirtual;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by tecnosim on 1/18/2018.
 */

public interface InMobRetrofitService {

    @POST(ConstInVirtual.LIST_AVISOS)
    Observable<List<AvisosBean>> getData(@Body BitacoraBean bitacoraBean);

    @POST(ConstInVirtual.SET_AVISOS)
    Observable<List<BitacoraBean>> setData(@Body List<AvisosBean> listAvisos);

    @GET(ConstInVirtual.LIST_TIPO_AVISOS)
    Observable<List<TipoAvisoBean>> getListTipoAvisos();

    @GET(ConstInVirtual.LIST_TRANSACCION_AVISOS)
    Observable<List<TransaccionAviso>> getListTransaccionAvisos();

    @POST(ConstInVirtual.SET_CUENTAS)
    Observable<CuentasBean> setCuentas(@Body CuentasBean cuentasBean);

    @POST(ConstInVirtual.UPDATE_CUENTAS)
    Observable<CuentasBean> updateCuentas(@Body CuentasBean cuentasBean);
}
