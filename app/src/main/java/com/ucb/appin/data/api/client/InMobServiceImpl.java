package com.ucb.appin.data.api.client;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.data.networking.InMobRetrofitClient;
import java.util.List;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tecnosim on 1/18/2018.
 */

public class InMobServiceImpl extends InMobRetrofitClient implements InMobService {

    @Override
    public Observable<List<AvisosBean>> listAvisos(BitacoraBean bitacoraBean) {
        return getInMobRetrofitService().getData(bitacoraBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<BitacoraBean>> setAvisos(List<AvisosBean> listAvisos) {
        return getInMobRetrofitService().setData(listAvisos)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<TipoAvisoBean>> listTipoAvisos() {
        return getInMobRetrofitService().getListTipoAvisos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<TransaccionAviso>> listTransaccionAvisos() {
        return getInMobRetrofitService().getListTransaccionAvisos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<CuentasBean> setCuentas(CuentasBean cuentasBean) {
        return getInMobRetrofitService().setCuentas(cuentasBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<CuentasBean> updateCuentas(CuentasBean cuentasBean) {
        return getInMobRetrofitService().updateCuentas(cuentasBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
