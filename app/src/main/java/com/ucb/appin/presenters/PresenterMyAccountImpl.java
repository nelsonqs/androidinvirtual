package com.ucb.appin.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.services.SynForegroundService;
import com.ucb.appin.util.UtilInmo;
import com.ucb.appin.views.activity.ActivityAdminAccount;

import io.realm.Realm;

/**
 * Created by Juan choque on 1/27/2018.
 */

public class PresenterMyAccountImpl implements PresenterMyAccount {
    private Context context;
    private ActivityAdminAccount activityAdminAccount;
    private UserAccountTask mAuthTask = null;

    public PresenterMyAccountImpl(Context context, ActivityAdminAccount activityAdminAccount) {
        this.context = context;
        this.activityAdminAccount = activityAdminAccount;
    }

    @Override
    public void addAccount(CuentasBean cuentasBean, int sw) {
        activityAdminAccount.showProgress(true);

        SynForegroundService synForegroundService = new SynForegroundService();
        synForegroundService.syncronizedCloudLocalSetCuentas(cuentasBean, this);

        //veriry if exist conexion to internet
        /*UtilInmo utilInmo = new UtilInmo(this.context);
        if(utilInmo.isOnline()){
            SynForegroundService synForegroundService = new SynForegroundService();
            synForegroundService.

            activityAdminAccount.showProgress(true);
            mAuthTask = new UserAccountTask(cuentasBean, 1);
            mAuthTask.execute((Void) null);
        }
        else{
            Toast.makeText(context, ConstInVirtual.MESSAGE_OFFLINE, Toast.LENGTH_LONG).show();
        }*/
    }

    @Override
    public void getAccount() {
        UtilInmo utilInmo = new UtilInmo(context);
        String imei = utilInmo.getNumberPhone();

        CuentasBean cuentasBean = new CuentasBean();
        cuentasBean.setTelefono(imei);

        activityAdminAccount.showProgress(true);
        mAuthTask = new UserAccountTask(cuentasBean, 0);
        mAuthTask.execute((Void) null);
    }

    @Override
    public void showProgress(boolean show) {
        activityAdminAccount.showProgress(show);
        activityAdminAccount.finish();
    }

    @Override
    public void showMessage(String messageOkOperation) {
        activityAdminAccount.showMessage(messageOkOperation);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class UserAccountTask extends AsyncTask<Void, Void, Boolean> {
        private Realm realm;

        private CuentasBean cuentasBean;
        private int operation = 0;
        private CuentasBean rCuentasBean;
        private CuentasBean nCuentasBean = null;

        public UserAccountTask(CuentasBean cuentasBean, int operation) {
            this.cuentasBean = cuentasBean;
            this.operation = operation;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            realm = Realm.getDefaultInstance();

            try {
                realm.beginTransaction();
                if(operation == 0){
                    rCuentasBean = null;
                    try {
                        rCuentasBean = realm.where(CuentasBean.class)
                                .equalTo("celular",this.cuentasBean.getTelefono())
                                .findFirst();

                        if(rCuentasBean != null){
                            nCuentasBean = new CuentasBean();
                            nCuentasBean.setId(rCuentasBean.getId());
                            nCuentasBean.setNombres(rCuentasBean.getNombres());
                            nCuentasBean.setAparteno(rCuentasBean.getAparteno());
                            nCuentasBean.setAmaterno(rCuentasBean.getAmaterno());
                        }

                    }catch (Exception e) {
                        rCuentasBean = null;
                    }
                }
                else if(operation == 1){
                    try {
                        realm.copyToRealmOrUpdate(this.cuentasBean);
                        Log.v(">>>>",">>REGISTRO CORRECTO>>");
                    }catch (Exception e) {
                        Log.v(">>>>",">>ERROR>>" + e.getMessage());
                    }

                }
               realm.commitTransaction();
            } catch (Exception e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            activityAdminAccount.showProgress(false);

            if (success) {
                if(operation == 1){
                    activityAdminAccount.finish();
                }
                else if(operation == 0){

                    activityAdminAccount.loadData(nCuentasBean);
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            activityAdminAccount.showProgress(false);
        }
    }
}
