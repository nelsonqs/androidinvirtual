package com.ucb.appin.presenters;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.CuentasBean;

/**
 * Created by Juan choque on 1/27/2018.
 */

public interface PresenterMyAccount {
    void addAccount(CuentasBean cuentasBean, int sw);

    void getAccount();

    void showProgress(boolean b);

    void showMessage(String messageOkOperation);
}
