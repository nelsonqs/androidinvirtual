package com.ucb.appin.presenters;

import com.ucb.appin.data.model.AvisosBean;

/**
 * Created by Juan choque on 1/27/2018.
 */

public interface PresenterPublication {
    void listAvisos(String tBuscar);

    void searchListPublication(String tBuscar);

    void showDetailPublication(AvisosBean avisosBean);

    void dispose();
}
