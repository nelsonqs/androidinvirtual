package com.ucb.appin.presenters;

import android.content.Context;
import android.util.Log;

import com.trello.rxlifecycle2.RxLifecycle;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.ucb.appin.data.api.client.InMobService;
import com.ucb.appin.data.api.client.InMovServiceLocalImpl;
import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.util.ConstInVirtual;
import com.ucb.appin.views.fragment.FragmentPublication;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Case;
import io.realm.Realm;
import io.realm.Sort;


public class PresenterPublicationImpl extends RxAppCompatActivity implements PresenterPublication {
    private Context context;
    private FragmentPublication fragmentPublication;

    private Realm realm;
    private InMobService inMobService;


    public PresenterPublicationImpl(Context context, FragmentPublication iPublicationsView) {
        this.context = context;
        this.fragmentPublication = iPublicationsView;
        this.realm = Realm.getDefaultInstance();
        this.inMobService = new InMovServiceLocalImpl();
    }

    @Override
    public void listAvisos(String tBuscar) {
        dispose();
        io.reactivex.Observable<List<AvisosBean>> observable = this.inMobService.listAvisos(new BitacoraBean());
        try {
            Disposable disposable = observable.compose(RxLifecycle.bindUntilEvent(lifecycle(), ActivityEvent.DESTROY))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .toFlowable(BackpressureStrategy.BUFFER)
                    .repeatWhen(
                            robservable -> robservable.delay(ConstInVirtual.SECOND_TO_REVIEW_NEW_DATA, TimeUnit.SECONDS)
                    )
                    .retry()
                    .subscribe(data -> {
                                Log.v("==>", "=>RETURN IN EQUIP>" + data.size());
                                this.fragmentPublication.loadData(data);
                            },
                            error -> {
                                Log.v("==>", "=>ERROR EQUIP>" + error.getMessage());
                            });

            ConstInVirtual.disposable.add(disposable);
        } catch (Exception er) {
            Log.i("---ERROR--->", ">>" + er.getMessage());
        }
    }

    @Override
    public void searchListPublication(String tBuscar) {
        dispose();
        io.reactivex.Observable<List<AvisosBean>> observable = this.inMobService.listAvisos(new BitacoraBean());
        try {
            Disposable disposable = observable.compose(RxLifecycle.bindUntilEvent(lifecycle(), ActivityEvent.DESTROY))
                    .debounce(ConstInVirtual.STOP_STRESS_USER_MULTIPLES_CLICK, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable(BackpressureStrategy.BUFFER)
                    .switchMap(textChangeEvent -> {
                        // Use Async API to move Realm queries off the main thread.
                        return realm.where(AvisosBean.class)
                                .contains ("titulo", tBuscar, Case.INSENSITIVE)
                                .findAllSortedAsync("id", Sort.DESCENDING)
                                .asFlowable();
                    })
                    .filter(notification -> notification.isLoaded())
                    .subscribe(data -> {
                                Log.v("==>", "=>RETURN IN EQUIP>" + data.size());
                                this.fragmentPublication.loadData(data);
                            },
                            error -> {
                                Log.v("==>", "=>ERROR EQUIP>" + error.getMessage());
                            });

            ConstInVirtual.disposable.add(disposable);
        } catch (Exception er) {
            Log.i("---ERROR--->", ">>" + er.getMessage());
        }
    }

    @Override
    public void showDetailPublication(AvisosBean avisosBean) {
        this.fragmentPublication.showDetailPublication(avisosBean);
    }

    @Override
    public void dispose() {
        try {
            ConstInVirtual.disposable.clear();
        } catch (Exception er) {

        }
    }
}
