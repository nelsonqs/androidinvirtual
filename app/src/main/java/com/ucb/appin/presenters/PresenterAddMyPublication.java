package com.ucb.appin.presenters;

import com.ucb.appin.data.model.AvisosBean;

/**
 * Created by Juan choque on 1/27/2018.
 */

public interface PresenterAddMyPublication {
    AvisosBean addAviso(AvisosBean avisosBean, int typeOperation);
    void listTipoAvisos();
    void listTransaccionAvisos();

    void editAviso(AvisosBean avisosBean);
}
