package com.ucb.appin.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.CuentasBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.services.localServices.SynLocalService;
import com.ucb.appin.services.localServices.SynLocalServiceImpl;
import com.ucb.appin.util.ConstInVirtual;
import com.ucb.appin.views.activity.ActivityAddMyPublication;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Juan choque on 1/27/2018.
 */

public class PresenterAddMyPublicationImpl implements PresenterAddMyPublication {
    private Context context;
    private ActivityAddMyPublication activityAddMyPublication;

    private AddMyPublicatonsTask mAuthTask = null;

    public PresenterAddMyPublicationImpl(Context context, ActivityAddMyPublication activityAddMyPublication) {
        this.context = context;
        this.activityAddMyPublication = activityAddMyPublication;
    }

    @Override
    public AvisosBean addAviso(AvisosBean avisosBean, int typeOperation) {
        try {
            if(avisosBean != null){
                activityAddMyPublication.showProgress(true);
                mAuthTask = new AddMyPublicatonsTask(avisosBean, typeOperation);
                mAuthTask.execute((Void) null);
            }
        }catch (Exception er){
            Log.i("---ERROR--->",">>" + er.getMessage());
            avisosBean = null;
        }
        return avisosBean;
    }

    @Override
    public void listTipoAvisos() {
        List<TipoAvisoBean> listTipoAvisos = new ArrayList<TipoAvisoBean>();

       // todo da error al descomentar favor revisar
        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<TipoAvisoBean> tipoAvisos = realm.where(TipoAvisoBean.class)
                    //.equalTo("estado", ConstantesDcompras.ACTIVO)
                    .findAllSorted("id", Sort.DESCENDING);

            if(tipoAvisos != null){
                for(int i = 0;i < tipoAvisos.size();i++){
                    TipoAvisoBean tipoAvisoBean = tipoAvisos.get(i);
                    TipoAvisoBean ntipoAvisoBean = new TipoAvisoBean();
                    ntipoAvisoBean.setId(tipoAvisoBean.getId());
                    ntipoAvisoBean.setIdc(tipoAvisoBean.getIdc());
                    ntipoAvisoBean.setNombre(tipoAvisoBean.getNombre());
                    ntipoAvisoBean.setEstado(tipoAvisoBean.getEstado());

                    listTipoAvisos.add(ntipoAvisoBean);
                }
            }
        }
        catch (Exception err){
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }

//        Log.v("...>",">L T A>" + listTipoAvisos.size());
//
//        TipoAvisoBean tipoAvisoBean = new TipoAvisoBean();
//        tipoAvisoBean.setId(1);
//        tipoAvisoBean.setNombre("Casa/Chalet");
//
//        TipoAvisoBean tipoAvisoBean0 = new TipoAvisoBean();
//        tipoAvisoBean0.setId(2);
//        tipoAvisoBean0.setNombre("Departamento");
//
//        TipoAvisoBean tipoAvisoBean1 = new TipoAvisoBean();
//        tipoAvisoBean1.setId(3);
//        tipoAvisoBean1.setNombre("Habitacion");
//
//        TipoAvisoBean tipoAvisoBean2 = new TipoAvisoBean();
//        tipoAvisoBean2.setId(4);
//        tipoAvisoBean2.setNombre("Local Comercial/Tienda");
//
//        TipoAvisoBean tipoAvisoBean3 = new TipoAvisoBean();
//        tipoAvisoBean3.setId(5);
//        tipoAvisoBean3.setNombre("Otros");
//
//        listTipoAvisos.add(tipoAvisoBean);
//        listTipoAvisos.add(tipoAvisoBean0);
//        listTipoAvisos.add(tipoAvisoBean1);
//        listTipoAvisos.add(tipoAvisoBean2);
//        listTipoAvisos.add(tipoAvisoBean3);

        this.activityAddMyPublication.loadInitDataTipoAviso(listTipoAvisos);
    }

    @Override
    public void listTransaccionAvisos() {
        List<TransaccionAviso>listTransaccionAvisos = new ArrayList<TransaccionAviso>();

//        todo favor revisar da error al descomentar
        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<TransaccionAviso> transaccionAvisos = realm.where(TransaccionAviso.class)
                    //.equalTo("estado", ConstantesDcompras.ACTIVO)
                    .findAllSorted("id", Sort.DESCENDING);

            if(transaccionAvisos != null){
                for(int i = 0;i < transaccionAvisos.size();i++){
                    TransaccionAviso transaccionAviso = transaccionAvisos.get(i);
                    TransaccionAviso ntransaccionAviso = new TransaccionAviso();
                    ntransaccionAviso.setId(transaccionAviso.getId());
                    ntransaccionAviso.setIdc(transaccionAviso.getIdc());
                    ntransaccionAviso.setNombre(transaccionAviso.getNombre());
                    ntransaccionAviso.setEstado(transaccionAviso.getEstado());

                    listTransaccionAvisos.add(ntransaccionAviso);
                }
            }
        }
        catch (Exception err){
        }
        finally {
            if(realm != null){
                realm.close();
            }
        }

//        TransaccionAviso transaccionAviso = new TransaccionAviso();
//        transaccionAviso.setId(1);
//        transaccionAviso.setNombre("Venta");
//
//        TransaccionAviso transaccionAviso0 = new TransaccionAviso();
//        transaccionAviso0.setId(1);
//        transaccionAviso0.setNombre("Alquiler");
//
//        TransaccionAviso transaccionAviso1 = new TransaccionAviso();
//        transaccionAviso1.setId(1);
//        transaccionAviso1.setNombre("Anticretico");
//
//        TransaccionAviso transaccionAviso2 = new TransaccionAviso();
//        transaccionAviso2.setId(1);
//        transaccionAviso2.setNombre("Otros");
//
//        listTransaccionAvisos.add(transaccionAviso);
//        listTransaccionAvisos.add(transaccionAviso0);
//        listTransaccionAvisos.add(transaccionAviso1);
//        listTransaccionAvisos.add(transaccionAviso2);


        this.activityAddMyPublication.loadInitDataTransaccionAviso(listTransaccionAvisos);
    }

    @Override
    public void editAviso(AvisosBean avisosBean) {

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class AddMyPublicatonsTask extends AsyncTask<Void, Void, Boolean> {
        private AvisosBean avisosBean;
        private Realm srealm;
        private int typeOperation = 0;

        AddMyPublicatonsTask(AvisosBean avisosBean, int typeOperation){
            this.avisosBean = avisosBean;
            this.typeOperation = typeOperation;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            srealm = Realm.getDefaultInstance();
            try {

                SynLocalService synLocalService = new SynLocalServiceImpl(context);

                if(typeOperation == ConstInVirtual.ADD_OPERACION){
                    int id = 0;
                    Number aux = srealm.where(AvisosBean.class)
                            .max("id");
                    if(aux != null){
                        id = aux.intValue();
                    }
                    int idAviso = id + 1;
                    avisosBean.setId(idAviso);
                }
                else if(typeOperation == ConstInVirtual.EDIT_OPERATION){
                    BitacoraBean bitacoraBean = synLocalService.getBitacoraByIdBean(avisosBean.getId());
                    if(bitacoraBean != null){
                        if(bitacoraBean.getEstado().equals("A")){
                            synLocalService.removeBitacoraById(bitacoraBean.getId());
                            typeOperation = ConstInVirtual.ADD_OPERACION;
                        }
                    }
                }

                //get account
                CuentasBean cuentasBean = synLocalService.getCuenta();

                if(cuentasBean != null){
                    avisosBean.setFecPublicacion(new Date());
                    avisosBean.setEstado(ConstInVirtual.ACTIVE);
                    avisosBean.setCuenta(cuentasBean.getId());

                    srealm.beginTransaction();
                    srealm.copyToRealmOrUpdate(avisosBean);
                    srealm.commitTransaction();

                    srealm.refresh();

                    //get id max id bitacora
                    int id = synLocalService.getMaxId();

                    Log.v("-->TYPE OF O ADD","-->" + typeOperation);
                    //test only
                    BitacoraBean bitacoraBean = new BitacoraBean();
                    int idBitacora = (id + 1);
                    bitacoraBean.setId(idBitacora);
                    bitacoraBean.setOperacion(typeOperation);// 1 = add, 2 = mod, 3 = del
                    bitacoraBean.setIdBean(avisosBean.getId());

                    synLocalService.insertBitacora(bitacoraBean);
                    //end test
                }
                else{
                    return false;
                }

                Log.i("---ADD CORRECTO--->",">>");
            }catch (Exception er){
                Log.i("---ERROR--->",">>" + er.getMessage());
                avisosBean = null;
                return false;
            }
            finally {
                if(srealm != null){
                    srealm.close();
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            activityAddMyPublication.showProgress(false);

            if (success) {
                activityAddMyPublication.showMessage(ConstInVirtual.INCORRECT_OPERATION);
                activityAddMyPublication.finish();
            }
            else{
                activityAddMyPublication.showMessage(ConstInVirtual.CORRECT_OPERATION);
                Toast.makeText(context, ConstInVirtual.MESSAGE_NOTHING_ACCOUNT, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            activityAddMyPublication.showProgress(false);
        }
    }
}
