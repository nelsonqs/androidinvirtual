package com.ucb.appin.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ucb.appin.data.model.AvisosBean;
import com.ucb.appin.data.model.BitacoraBean;
import com.ucb.appin.data.model.TipoAvisoBean;
import com.ucb.appin.data.model.TransaccionAviso;
import com.ucb.appin.services.localServices.SynLocalService;
import com.ucb.appin.services.localServices.SynLocalServiceImpl;
import com.ucb.appin.util.ConstInVirtual;
import com.ucb.appin.views.fragment.FragmentMyPublication;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Juan choque on 1/27/2018.
 */

public class PresenterMyPublicationImpl implements PresenterMyPublication {
    private Context context;
    private FragmentMyPublication fragmentMyPublication;

    private Realm realm;
    private MyPublicatonsTask mAuthTask = null;
    private MyPublicatonsDeleteTask mAuthDeleteTask = null;


    public PresenterMyPublicationImpl(Context context, FragmentMyPublication fragmentMyPublication) {
        this.context = context;
        this.fragmentMyPublication = fragmentMyPublication;
        this.realm = Realm.getDefaultInstance();
    }

    @Override
    public void listAvisos(String tBuscar) {
        try {
            fragmentMyPublication.showProgress(true);
            mAuthTask = new MyPublicatonsTask(tBuscar, this);
            mAuthTask.execute((Void) null);
        }catch (Exception er){
            Log.i("---ERROR--->",">>" + er.getMessage());
        }
    }



    public void listAvisos(List<AvisosBean>rListAvisos) {
        try {
            Log.i("------>",">ENTRA EN LISTA  PRESENTER>");
            fragmentMyPublication.loadData(rListAvisos);
        }catch (Exception er){
            Log.i("---ERROR--->",">>" + er.getMessage());
        }
    }



    @Override
    public void confirmDeleteAviso(AvisosBean avisosBean) {
        this.fragmentMyPublication.showDialogDelete(avisosBean);
    }

    @Override
    public AvisosBean deleteAviso(AvisosBean avisosBean) {
        try {
            Log.v("--->",">>" + avisosBean.getId());
            mAuthDeleteTask = new MyPublicatonsDeleteTask(avisosBean);
            mAuthDeleteTask.execute((Void) null);
        }catch (Exception e){
        }
        return avisosBean;
    }

    @Override
    public void showEditAviso(AvisosBean avisosBean) {
        this.fragmentMyPublication.showEditAviso(avisosBean);
    }

    @Override
    public void listTipoAvisos() {
        List<TipoAvisoBean> listTipoAvisos = new ArrayList<TipoAvisoBean>();

        TipoAvisoBean tipoAvisoBean = new TipoAvisoBean();
        tipoAvisoBean.setId(1);
        tipoAvisoBean.setNombre("Casa/Chalet");

        TipoAvisoBean tipoAvisoBean0 = new TipoAvisoBean();
        tipoAvisoBean0.setId(2);
        tipoAvisoBean0.setNombre("Departamento");

        TipoAvisoBean tipoAvisoBean1 = new TipoAvisoBean();
        tipoAvisoBean1.setId(3);
        tipoAvisoBean1.setNombre("Habitacion");

        TipoAvisoBean tipoAvisoBean2 = new TipoAvisoBean();
        tipoAvisoBean2.setId(4);
        tipoAvisoBean2.setNombre("Local Comercial/Tienda");

        TipoAvisoBean tipoAvisoBean3 = new TipoAvisoBean();
        tipoAvisoBean3.setId(5);
        tipoAvisoBean3.setNombre("Otros");

        listTipoAvisos.add(tipoAvisoBean);
        listTipoAvisos.add(tipoAvisoBean0);
        listTipoAvisos.add(tipoAvisoBean1);
        listTipoAvisos.add(tipoAvisoBean2);
        listTipoAvisos.add(tipoAvisoBean3);

        this.fragmentMyPublication.loadDataTipoAviso(listTipoAvisos);
    }

    @Override
    public void listTransaccionAvisos() {
        List<TransaccionAviso>listTransaccionAvisos = new ArrayList<TransaccionAviso>();

        TransaccionAviso transaccionAviso = new TransaccionAviso();
        transaccionAviso.setId(1);
        transaccionAviso.setNombre("Venta");

        TransaccionAviso transaccionAviso0 = new TransaccionAviso();
        transaccionAviso0.setId(1);
        transaccionAviso0.setNombre("Alquiler");

        TransaccionAviso transaccionAviso1 = new TransaccionAviso();
        transaccionAviso1.setId(1);
        transaccionAviso1.setNombre("Anticretico");

        TransaccionAviso transaccionAviso2 = new TransaccionAviso();
        transaccionAviso2.setId(1);
        transaccionAviso2.setNombre("Otros");

        listTransaccionAvisos.add(transaccionAviso);
        listTransaccionAvisos.add(transaccionAviso0);
        listTransaccionAvisos.add(transaccionAviso1);
        listTransaccionAvisos.add(transaccionAviso2);

        this.fragmentMyPublication.loadDataTransaccionAviso(listTransaccionAvisos);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class MyPublicatonsTask extends AsyncTask<Void, Void, Boolean> {
        private List<AvisosBean>rListAvisos;
        private Realm srealm;
        private String tbuscar;

        private PresenterMyPublicationImpl presenterMyPublicationImpl;

        MyPublicatonsTask(String tbuscar, PresenterMyPublicationImpl presenterMyPublicationImpl) {
            rListAvisos = new ArrayList<AvisosBean>();
            this.tbuscar = tbuscar;
            this.presenterMyPublicationImpl = presenterMyPublicationImpl;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            srealm = Realm.getDefaultInstance();
            srealm.beginTransaction();
            try {
                //rListAvisos = this.inMovServ.getAllAdvicesAsync(srealm, tbuscar, ConstantAppInVirtual.LOCAL_LOCATION);
                rListAvisos = new ArrayList<AvisosBean>();

                RealmResults<AvisosBean> avisos = srealm.where(AvisosBean.class)
                        .equalTo("estado",ConstInVirtual.ACTIVE)
                        .findAllSorted("id", Sort.DESCENDING);

                Log.v(">>>>",">>>ANTES>>>" + avisos.size());

                if(avisos != null){
                    for(int i = 0;i < avisos.size();i++){
                        AvisosBean rAvisosBean = avisos.get(i);
                        AvisosBean avisosBean = new AvisosBean();
                        avisosBean.setId(rAvisosBean.getId());
                        avisosBean.setIdc(rAvisosBean.getIdc());
                        avisosBean.setTitulo(rAvisosBean.getTitulo());
                        avisosBean.setDescripcion(rAvisosBean.getDescripcion());
                        avisosBean.setPrecio(rAvisosBean.getPrecio());
                        avisosBean.setTelefono(rAvisosBean.getTelefono());
                        avisosBean.setDireccion(rAvisosBean.getDireccion());
                        avisosBean.setLatitud(rAvisosBean.getLatitud());
                        avisosBean.setLongitud(rAvisosBean.getLongitud());
                        avisosBean.setTipoaviso(rAvisosBean.getTipoaviso());
                        avisosBean.setTransaccionAviso(rAvisosBean.getTransaccionaviso());
                        avisosBean.setImagen(rAvisosBean.getImagen());

                        rListAvisos.add(avisosBean);
                    }
                }

                srealm.commitTransaction();
                Log.v("=====>",">>C AVISOS>>" + rListAvisos.size());
            }catch (Exception er){
                Log.v("=====>",">ERROR>" + er.getMessage());
                return false;
            }
            finally {
                if(srealm != null){
                    srealm.close();
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            fragmentMyPublication.showProgress(false);
            Log.v("=>>>>>",success + "<<<>>>" + rListAvisos.size());
            if (success) {
                presenterMyPublicationImpl.listAvisos(rListAvisos);
                fragmentMyPublication.finish();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            fragmentMyPublication.showProgress(false);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    public class MyPublicatonsDeleteTask extends AsyncTask<Void, Void, Boolean> {
        private Realm srealm;
        private AvisosBean avisosBean;

        public MyPublicatonsDeleteTask(AvisosBean avisosBean) {
            this.avisosBean = avisosBean;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            srealm = Realm.getDefaultInstance();
            srealm.beginTransaction();
            try {
                boolean deleted = false;
                AvisosBean rAvisosBean = srealm.where(AvisosBean.class)
                        .equalTo("id",avisosBean.getId())
                        .findFirst();

                if(rAvisosBean != null){

                    rAvisosBean.setEstado(ConstInVirtual.INACTIVE);//for delete
                    srealm.copyToRealmOrUpdate(rAvisosBean);
                    srealm.commitTransaction();

                    SynLocalService synLocalService = new SynLocalServiceImpl();

                    BitacoraBean rbitacoraBean = synLocalService.getBitacoraByIdBean(rAvisosBean.getId());
                    if(rbitacoraBean != null){
                        if(rbitacoraBean.getEstado().equals("A")){
                            synLocalService.removeBitacoraById(rbitacoraBean.getId());
                            deleted = true;
                        }
                    }

                    if(!deleted){
                        //get id max id bitacora
                        int id = synLocalService.getMaxId();

                        //test only
                        BitacoraBean bitacoraBean = new BitacoraBean();
                        int idBitacora = (id + 1);
                        bitacoraBean.setId(idBitacora);
                        bitacoraBean.setOperacion(3);// 1 = add, 2 = mod, 3 = del
                        bitacoraBean.setIdBean(rAvisosBean.getId());

                        synLocalService.insertBitacora(bitacoraBean);
                        //end test
                    }
                    Log.v("=====>",">>>ELIMINACION CORRECTA>");
                }
            }catch (Exception er){
                Log.v("=====>",">DELETE ERROR>" + er.getMessage());
                return false;
            }
            finally {
                if(srealm != null){
                    srealm.close();
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthDeleteTask = null;
            if (success) {
                listAvisos("");
            }
        }

        @Override
        protected void onCancelled() {
            mAuthDeleteTask = null;
        }
    }

}
